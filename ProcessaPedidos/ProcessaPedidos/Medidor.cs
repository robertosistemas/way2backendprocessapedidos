﻿using System;
using System.Collections.Generic;

namespace ProcessaPedidos
{
    /// <summary>
    /// Informações relativa a cada medidor que será consultado
    /// </summary>
    public class Medidor
    {

        private string _ip;

        /// <summary>
        /// Endereço IP do Medidor
        /// </summary>
        public string Ip
        {
            get
            {
                return _ip;
            }
            set
            {
                _ip = value;
            }
        }

        private string _porta;

        /// <summary>
        /// Porta de comunição do Medidor
        /// </summary>
        public string Porta
        {
            get
            {
                return _porta;
            }
            set
            {
                _porta = value;
            }
        }

        private string _instanteInicial;

        /// <summary>
        /// Instante Inicial para a leitura de dados
        /// </summary>
        public string InstanteInicial
        {
            get
            {
                return _instanteInicial;
            }
            set
            {
                _instanteInicial = value;
            }
        }

        private string _numeroSerie;

        /// <summary>
        /// Número de Série do Médidor
        /// </summary>
        public string NumeroSerie
        {
            get
            {
                return _numeroSerie;
            }
            set
            {
                _numeroSerie = value;
            }
        }

        private DateTime _dataHora;

        /// <summary>
        /// Data e Hora da Leitura do Medidor
        /// </summary>
        public DateTime DataHora
        {
            get
            {
                return _dataHora;
            }
            set
            {
                _dataHora = value;
            }
        }

        private bool _sucessoComunicao;

        /// <summary>
        /// Indica se houve sucesso ou não na comunicação com o medidor
        /// </summary>
        public bool SucessoComunicao
        {
            get
            {
                return _sucessoComunicao;
            }
            set
            {
                _sucessoComunicao = value;
            }
        }

        private int _sequencia;

        /// <summary>
        /// Sequência do medidor dentro do arquivo da lista de pedidos
        /// </summary>
        public int Sequencia
        {
            get
            {
                return _sequencia;
            }
            set
            {
                _sequencia = value;
            }
        }

        /// <summary>
        /// Retorna o Instante Inicial no tipo DateTime
        /// </summary>
        /// <returns></returns>
        public DateTime instanteInicialAsDateTime()
        {
            int ano;
            int mes;
            int dia;
            int hora;
            int minuto;
            int segundo = 0;

            int.TryParse(this.InstanteInicial.Substring(0, 4), out ano);
            int.TryParse(this.InstanteInicial.Substring(5, 2), out mes);
            int.TryParse(this.InstanteInicial.Substring(8, 2), out dia);
            int.TryParse(this.InstanteInicial.Substring(11, 2), out hora);
            int.TryParse(this.InstanteInicial.Substring(14, 2), out minuto);

            return new DateTime(ano, mes, dia, hora, minuto, segundo);
        }

        List<Registro> _registros;

        /// <summary>
        /// Lista de Registro de valores lidos para o medidor
        /// </summary>
        public List<Registro> Registros
        {
            get
            {
                if (_registros == null)
                {
                    _registros = new List<Registro>();
                }
                return _registros;
            }
            set
            {
                _registros = value;
            }
        }
    }
}
