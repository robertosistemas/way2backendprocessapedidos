﻿using System;
using System.Text;

namespace ProcessaPedidos
{
    /// <summary>
    /// Funções acessórias
    /// </summary>
    public static class Util
    {

        /// <summary>
        /// Completa um string com um caractar a direita ou a esquerda até o tamanho informado
        /// </summary>
        /// <param name="texto">Texto a Completar</param>
        /// <param name="character">Catacter usado para completar</param>
        /// <param name="tamanho">Tamanho Máximo</param>
        /// <param name="local">L para completar a Esquerda e R para completar a direita</param>
        /// <returns></returns>
        public static string paddingChar(string texto, char character, int tamanho, char local)
        {
            string retorno = texto;
            while (retorno.Length < tamanho)
            {
                if (local == 'L')
                {
                    retorno = string.Concat(character, retorno);
                }
                else
                {
                    retorno = string.Concat(retorno, character);
                }
            }
            return retorno;
        }

        /// <summary>
        /// Converte uma Matriz de bytes para uma string com a base especificado
        /// </summary>
        /// <param name="arr">Matriz de bytes a converter</param>
        /// <param name="inicio">Posição inicial no array</param>
        /// <param name="fim">Posição final no array</param>
        /// <param name="baseNumero">Base numérica para conversão 2 4 8 10 ou 16</param>
        /// <param name="padding">Tamanho para preencher com 0 a esquerda</param>
        /// <param name="paddingLocal">Indica se o local do preenchimento sera L esquerda ou R direita</param>
        /// <returns></returns>
        public static string converteBytesToString(byte[] arr, int inicio, int fim, int baseNumero = 2, int padding = 0, char paddingLocal = 'L')
        {
            StringBuilder sb = new StringBuilder();
            for (int i = inicio; i <= fim; i++)
            {
                if (padding > 0)
                {
                    sb.Append(paddingChar(Convert.ToString(arr[i], baseNumero), '0', padding, paddingLocal));
                }
                else
                {
                    sb.Append(Convert.ToString(arr[i], baseNumero));
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// Converte uma data e Hora para uma string com base binária
        /// </summary>
        /// <param name="instante">Converte um instante em uma representação binária</param>
        /// <returns></returns>
        public static string converteDataHoraToBinario(DateTime instante)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(paddingChar(Convert.ToString(instante.Year, 2), '0', 12, 'L'));
            sb.Append(paddingChar(Convert.ToString(instante.Month, 2), '0', 4, 'L'));
            sb.Append(paddingChar(Convert.ToString(instante.Day, 2), '0', 5, 'L'));
            sb.Append(paddingChar(Convert.ToString(instante.Hour, 2), '0', 5, 'L'));
            sb.Append(paddingChar(Convert.ToString(instante.Minute, 2), '0', 6, 'L'));
            sb.Append(paddingChar(Convert.ToString(instante.Second, 2), '0', 6, 'L'));
            return string.Format("{0}11", sb.ToString());
        }

        /// <summary>
        /// Converte uma string binária para Data e hora
        /// </summary>
        /// <param name="instante">Converte uma representação binária de um instante em data e hora</param>
        /// <returns></returns>
        public static DateTime converteBinarioToDataHora(string instante)
        {
            int Posicao = 0;
            string BinAno = instante.Substring(Posicao, 12);
            Posicao += 12;
            string BinMes = instante.Substring(Posicao, 4);
            Posicao += 4;
            string BinDia = instante.Substring(Posicao, 5);
            Posicao += 5;
            string BinHora = instante.Substring(Posicao, 5);
            Posicao += 5;
            string BinMinuto = instante.Substring(Posicao, 6);
            Posicao += 6;
            string BinSegundo = instante.Substring(Posicao, 6);

            int ano = Convert.ToInt32(BinAno, 2);
            int mes = Convert.ToInt32(BinMes, 2);
            int dia = Convert.ToInt32(BinDia, 2);
            int hora = Convert.ToInt32(BinHora, 2);
            int minuto = Convert.ToInt32(BinMinuto, 2);
            int segundo = Convert.ToInt32(BinSegundo, 2);

            return new DateTime(ano, mes, dia, hora, minuto, segundo);
        }

        /// <summary>
        /// Retorna uma parte de uma matriz como sendo uma outra matriz
        /// </summary>
        /// <param name="arr">matriz origem</param>
        /// <param name="indice">posição inicial</param>
        /// <param name="tamanho">tamanho máximo</param>
        /// <returns></returns>
        public static byte[] subArray(byte[] arr, int indice, int tamanho)
        {
            byte[] retorno = new byte[tamanho];
            for (int i = 0; i < tamanho; i++)
            {
                retorno[i] = arr[i + indice];
            }
            return retorno;
        }

        /// <summary>
        /// Converte uma string binária em uma matriz de 5 elementos de bytes
        /// </summary>
        /// <param name="instante">String contendo o instante codificado em binário</param>
        /// <returns></returns>
        public static byte[] converteBinarioToArray(string instante)
        {
            byte[] retorno = new byte[5];

            int Posicao = 0;
            string pos1 = instante.Substring(Posicao, 8);
            Posicao += 8;
            string pos2 = instante.Substring(Posicao, 8);
            Posicao += 8;
            string pos3 = instante.Substring(Posicao, 8);
            Posicao += 8;
            string pos4 = instante.Substring(Posicao, 8);
            Posicao += 8;
            string pos5 = instante.Substring(Posicao, 8);

            retorno[0] = Convert.ToByte(pos1, 2);
            retorno[1] = Convert.ToByte(pos2, 2);
            retorno[2] = Convert.ToByte(pos3, 2);
            retorno[3] = Convert.ToByte(pos4, 2);
            retorno[4] = Convert.ToByte(pos5, 2);

            return retorno;
        }

        /// <summary>
        /// Obtem o checkSum de um Matriz de bytes calculando o XOR de cada byte excluíndo o cabeçalho
        /// </summary>
        /// <param name="arr">Matriz para calcular e obter um checkSum</param>
        /// <param name="maxPos">Posição Máxima</param>
        /// <returns></returns>
        public static byte obterChecksum(byte[] arr, int maxPos)
        {
            byte xOrValue = 0;
            for (int i = 1; i <= maxPos - 2; i++)
            {
                xOrValue ^= arr[i];
            }
            return xOrValue;
        }

        /// <summary>
        /// calculando o XOR de cada byte excluíndo o cabeçalho e define o resulta como checksum
        /// </summary>
        /// <param name="arr">Matriz para definir o CheckSum</param>
        public static void definirChecksum(ref byte[] arr)
        {
            byte xOrValue = 0;
            for (int i = 1; i <= arr.Length - 2; i++)
            {
                xOrValue ^= arr[i];
            }
            arr[arr.Length - 1] = xOrValue;
        }

        /// <summary>
        /// Copia informações de um Matriz para outro entre as posições inicial e final
        /// </summary>
        /// <param name="origem">Matriz de Origem</param>
        /// <param name="destino">Matriz de Destino</param>
        /// <param name="posInicial">Posição inicial</param>
        /// <param name="posFinal">Posição final</param>
        public static void copiaBytes(byte[] origem, ref byte[] destino, int posInicial, int posFinal)
        {
            for (int i = posInicial; i <= posFinal; i++)
            {
                destino[i] = origem[i];
            }
        }

        /// <summary>
        /// Arredonda para o decimal mais próximo com metade arredondando ao valor par
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static string arredondaMetade(string valor)
        {
            string retorno = valor.Substring(valor.IndexOf(',') + 1);
            retorno = paddingChar(retorno, '0', 3, 'D');
            if (retorno.EndsWith("5") && !"9".Contains(retorno.Substring(1, 1)))
            {
                short numero = Convert.ToInt16(retorno.Substring(1, 1));
                while (numero % 2 != 0)
                {
                    numero += 1;
                }
                retorno = string.Concat(valor.Substring(0, valor.IndexOf(',') + 1), retorno.Substring(0, 1), numero.ToString());
            }
            else
            {
                retorno = valor;
            }
            return retorno;
        }

    }
}
