﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace ProcessaPedidos
{
    class Program
    {

        private static string arquivoEntrada = string.Empty;
        private static List<Medidor> medidores = new List<Medidor>();

        static void Main(string[] args)
        {
            // Configura a informações da Cultura para pt-BR permitindo a correta formação e apresentação de dados
            System.Globalization.CultureInfo.DefaultThreadCurrentCulture = System.Globalization.CultureInfo.GetCultureInfo("pt-BR");
            System.Globalization.CultureInfo.DefaultThreadCurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo("pt-BR");

            // Processa os argumentos de linha de comando
            processaArgs(args);
            
        }

        /// <summary>
        /// Processa os parâmetros informados na execução do programa
        /// </summary>
        /// <param name="args"></param>
        static void processaArgs(string[] args)
        {
            if (args != null && args.Length > 0 && !string.IsNullOrWhiteSpace(args[0]))
            {
                arquivoEntrada = args[0];
            }
            else
            {
                // Verifica se há algum debugador anexado, se houver significa que está executando a partir do IDE do Visual Studio
                if (Debugger.IsAttached)
                {
                    // Atribui um nome de arquivo padrão para Testes
                    arquivoEntrada = "listaDePedidos.txt";
                }
            }

            if (string.IsNullOrWhiteSpace(arquivoEntrada))
            {
                // Informa a forma correta de utilizar o programa
                Console.WriteLine("Exemplo de Utilização:");
                Console.WriteLine("ProcessaPedidos.exe listaDePedidos.txt");
                Console.WriteLine(" ");
                Console.WriteLine("Pressione qualquer tecla para finalizar");
                Console.ReadKey();
            }
            else
            {

                FileInfo infoArq = new FileInfo(arquivoEntrada);

                if (infoArq.Exists)
                {
                    // Caso o arquivo informado exista, então carrega as informações dos medidores
                    arquivoEntrada = infoArq.FullName;

                    obtemMedidores(); // Obtem a lista de medidores

                    lerMedidores(); // Lê os registros dos medidores em PARALELO

                }
                else
                {
                    arquivoEntrada = string.Empty;
                    Console.WriteLine("É necessário informar um arquivo válido");
                    Console.WriteLine(" ");
                    Console.WriteLine("Pressione qualquer tecla para finalizar");
                    Console.ReadKey();
                }
            }
        }

        /// <summary>
        /// Cada informações dos medidores a partir do arquivo informado
        /// </summary>
        static void obtemMedidores()
        {
            medidores = new List<Medidor>();
            FileInfo fi = new FileInfo(arquivoEntrada);
            if (fi.Exists)
            {
                using (StreamReader sr = new StreamReader(fi.FullName))
                {
                    int sequencia = 1;
                    string[] itens;
                    string linha = sr.ReadLine();
                    while (linha != null)
                    {

                        itens = linha.Split(';');

                        if (itens.Length == 3)
                        {

                            Medidor medidor = new Medidor();
                            medidor.Ip = itens[0];
                            medidor.Porta = itens[1];
                            medidor.InstanteInicial = itens[2];
                            medidor.Sequencia = sequencia;
                            medidores.Add(medidor);

                            sequencia += 1;

                        }
                        linha = sr.ReadLine();
                    }
                }
            }
        }

        /// <summary>
        /// Salva os dados lidos dos medidores.
        /// Para cada medidor haverá um arquivo identificado por medidor_ + IP + Porta.csv contendo os dados lidos
        /// </summary>
        static void salvaDadosMedidores()
        {
            foreach (Medidor medi in medidores)
            {

                string arquivoSaida = string.Format("medidor_{0}_{1}.csv", medi.Ip.Replace(".", ""), medi.Porta);
                FileInfo fi = new FileInfo(arquivoSaida);

                // Se o arquivo já existe, exclui para criar novamente
                if (fi.Exists)
                {
                    System.IO.File.Delete(fi.FullName);
                }

                // Verifica Se houve sucesso na comunicação com o medidor, se houve, então inicia salvamento de dados
                if (medi.SucessoComunicao)
                {
                    using (StreamWriter sr = new StreamWriter(fi.FullName))
                    {
                        // Na primeira linha do arquivo, salva a informação do Número de Série e Data e Hora do Medidor
                        string linha = string.Format("{0};{1}", medi.NumeroSerie, medi.DataHora.ToString("yyyy-MM-dd HH:mm:ss"));
                        sr.WriteLine(linha);

                        // Interage por todos os registros do medidor
                        foreach (Registro regi in medi.Registros)
                        {
                            // Salva Data e Hora do registro e Valor do Registro
                            linha = string.Format("{0};{1}", regi.DataHora.ToString("yyyy-MM-dd HH:mm:ss"), regi.Valor);
                            sr.WriteLine(linha);
                        }

                    }
                }
            }

        }

        /// <summary>
        /// Processa a lista de Medidores em Paralelo Usando a TPL
        /// </summary>
        static void lerMedidores()
        {

            if (medidores.Count > 0)
            {
                Stopwatch sw = Stopwatch.StartNew();

                // Executa em paralelo
                Parallel.ForEach(medidores, (medi) =>
                {
                    Leitor leitor = new Leitor(medi);
                    leitor.leMedidor();
                });

                sw.Stop();

                // Salva os dados Lido do medidos
                salvaDadosMedidores();

                // Informa tempo gasto
                Console.WriteLine(" ");
                Console.WriteLine("Tempo gasto para realizar as leituras: {0}", sw.Elapsed.Minutes);
                Console.WriteLine(" ");
                Console.WriteLine("Pressione qualquer tecla para finalizar ");
                Console.ReadKey();

            }

        }

    }

}