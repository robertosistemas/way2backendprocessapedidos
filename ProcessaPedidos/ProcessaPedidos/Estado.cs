﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
// Novo
namespace ProcessaPedidos
{
    /// <summary>
    /// Guarda o estado de comunição de cada medidor
    /// </summary>
    public class Estado
    {

        // Medidor
        public Medidor Medidor = null;

        public IPEndPoint EndPoint = null;

        // Cliente socket.
        public Socket SocketCliente = null;

        // Tamanho do buffer para envio e recebimento de dados.
        public const int DadosTamanho = 128;

        // buffer de recebimento
        public byte[] DadosEnviar = new byte[DadosTamanho];

        // buffer de envio
        public byte[] DadosRecebidos = new byte[DadosTamanho];

        // Quantidade de Bytes enviados
        public int QuantidadesBytesEnviados;

        // Quantidade de Bytes Recebidos
        public int QuantidadesBytesRecebidos;

        // Descrição da função que está sendo executado
        public string DescricaoFuncao;

        // Instante que está sendo consultado no medidor
        public DateTime Instante;

        // Contador para saber que instante está sendo consultado
        public int NumeroInstante;

    }
}
