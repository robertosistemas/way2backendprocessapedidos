﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Globalization;

namespace ProcessaPedidos
{
    /// <summary>
    /// Realiza leituras dos medidores em paralelo
    /// </summary>
    public class Leitor
    {

        // Semáfaros para controle do fluxo de execução das Threads de comunicação com os Medidores
        private ManualResetEvent semafaroConexao = new ManualResetEvent(false);
        private ManualResetEvent semafaroEnvio = new ManualResetEvent(false);
        private ManualResetEvent semafaroRecebimento = new ManualResetEvent(false);
        private ManualResetEvent semafaroDesconexao = new ManualResetEvent(false);

        // Constante com formatação máxima de decimais para valore de ponto flutuante
        private const string floatFixedPoint = "0.###################################################################################################################################################################################################################################################################################################################################################";
        private const string formatacaoDados = "############################################0.00";

        // Tempo máximo para espera de resposta do Medidor
        private const int timerOut = 300000; // 5 Minutos

        // Instancia a ser processada pelo Leitor
        private Medidor _medidorAtual;

        /// <summary>
        /// Construtor que recebe uma instância de um Medidor a ser lido
        /// </summary>
        /// <param name="pMedidor"></param>
        public Leitor(Medidor pMedidor)
        {
            _medidorAtual = pMedidor;
        }

        /// <summary>
        /// Faz a leitura de um medidor
        /// </summary>
        public void leMedidor()
        {

            // Matrizes com os Frames de comunicação com os medidores
            byte[] solicitacaoDeConexao = { 0x1F, 0x01, 0x00, 0x00, 0x01 };
            byte[] lerNumeroDeSerie = { 0x1F, 0x02, 0x00, 0x00, 0x02 };
            byte[] lerDataHora = { 0x1F, 0x03, 0x00, 0x00, 0x03 };
            byte[] lerUmRegistro = { 0x1F, 0x05, 0x06, 0x01, 0x7D, 0xF2, 0xBA, 0x9E, 0x03, 0xAA };
            byte[] desconectarMedidor = { 0x1F, 0x06, 0x00, 0x00, 0x00 };

            // Define o CheckSum para o Frame de Desconexão com o Medidor
            Util.definirChecksum(ref desconectarMedidor);

            try
            {

                //string servidor = "way2testes.cloudapp.net";
                //IPHostEntry ipHostInfo = Dns.GetHostEntry(servidor);
                //IPAddress ipAddress = ipHostInfo.AddressList[0];

                // Obtêm o IP do Medidor
                IPAddress ipAddress = IPAddress.Parse(_medidorAtual.Ip);
                // Obtêm a Porta do Medidor
                int port = Convert.ToInt32(_medidorAtual.Porta);

                //Define um EndPoit a partir do IP e da porta
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

                // Cria uma instância do objeto Socket para comunicação com o medidor
                using (Socket clienteSocket = new Socket(remoteEP.Address.AddressFamily, SocketType.Stream, ProtocolType.Tcp))
                {

                    // Define o tempo máximo para resposta tanto para envio como para recebimento
                    clienteSocket.SendTimeout = timerOut;
                    clienteSocket.ReceiveTimeout = timerOut;

                    // Cria o Objeto para guardar o estado da comunicação
                    Estado estado = new Estado();

                    // Seta o EndPoint, Socket e Medidor
                    estado.EndPoint = remoteEP;
                    estado.SocketCliente = clienteSocket;
                    estado.Medidor = _medidorAtual;

                    // Conecta no servidor
                    connecta(estado);
                    semafaroConexao.WaitOne(timerOut); // Aguarda a conexão

                    // Verifica se conseguiu conectar ao servidor
                    if (estado.SocketCliente.Connected)
                    {

                        // Estabelece Conexão com o medidor
                        estado.DescricaoFuncao = "Conectar ao medidor";
                        estado.DadosEnviar = solicitacaoDeConexao;

                        envia(estado); // Envia dados
                        semafaroEnvio.WaitOne(timerOut); // Aguarda a conclusão do envio
                        mostraEnviados(estado); // Mostra dados enviados

                        recebe(estado);
                        semafaroRecebimento.WaitOne(timerOut);
                        mostraRecebidos(estado);

                        // Ler Número de Série
                        estado.DescricaoFuncao = "Ler Número de Série";
                        estado.DadosEnviar = lerNumeroDeSerie;

                        envia(estado);
                        semafaroEnvio.WaitOne(timerOut);
                        mostraEnviados(estado);

                        recebe(estado);
                        semafaroRecebimento.WaitOne(timerOut);
                        if (mostraRecebidos(estado))
                        {
                            _medidorAtual.NumeroSerie = Encoding.ASCII.GetString(estado.DadosRecebidos, 3, estado.DadosRecebidos[2]);
                        }

                        // Ler Data e Hora
                        estado.DescricaoFuncao = "Ler Data e Hora";
                        estado.DadosEnviar = lerDataHora;

                        envia(estado);
                        semafaroEnvio.WaitOne(timerOut);
                        mostraEnviados(estado);

                        recebe(estado);
                        semafaroRecebimento.WaitOne(timerOut);
                        if (mostraRecebidos(estado))
                        {
                            _medidorAtual.DataHora = Util.converteBinarioToDataHora(Util.converteBytesToString(estado.DadosRecebidos, 3, 7, 2, 8)); ;
                        }

                        DateTime instanteInicial = _medidorAtual.instanteInicialAsDateTime();

                        int semana = 7; // uma semana
                        int instantesPorDia = 288; // Instantes por dia (intervalos de 5 minutos
                        int totalLeituras = semana * instantesPorDia; // Total de leitura em uma semana

                        for (int b = 0; b < totalLeituras; b++)
                        {
                            byte[] valores = Util.converteBinarioToArray(Util.converteDataHoraToBinario(instanteInicial));
                            lerUmRegistro[4] = valores[0];
                            lerUmRegistro[5] = valores[1];
                            lerUmRegistro[6] = valores[2];
                            lerUmRegistro[7] = valores[3];
                            lerUmRegistro[8] = valores[4];

                            Util.definirChecksum(ref lerUmRegistro);

                            // Ler um Registro
                            estado.Instante = instanteInicial;
                            estado.NumeroInstante = b + 1;
                            estado.DescricaoFuncao = "Ler um Registro";
                            estado.DadosEnviar = lerUmRegistro;

                            envia(estado);
                            semafaroEnvio.WaitOne(timerOut);
                            mostraEnviados(estado);

                            recebe(estado);
                            semafaroRecebimento.WaitOne(timerOut);
                            if (mostraRecebidos(estado))
                            {
                                // Ler Valores e gravar
                                Registro registro = new Registro();

                                // Data e Hora do Registro
                                registro.DataHora = instanteInicial;

                                // Valor do Registro
                                byte[] valorBytes = Util.subArray(estado.DadosRecebidos, 4, 4);

                                // Se a plataforma de Hardware for Little Endian, tem que inverter o array antes de converter para Float
                                //if (BitConverter.IsLittleEndian)
                                //{
                                Array.Reverse(valorBytes); //Tenho que inverter sempre porque a plataforma de hardware do servidor sempre enviará invertido
                                //}

                                float valorFloat = BitConverter.ToSingle(valorBytes, 0);
                                float valorFloatDec = valorFloat / 1000; // Divido por 1000 para ficar com 3 casas decimais após a virgula

                                valorFloatDec = Convert.ToSingle(Util.arredondaMetade(valorFloatDec.ToString("########0.000")));

                                // MidpointRounding.ToEven = Quando um número está a meio caminho entre duas outras, ele é arredondado para o número par mais próximo.
                                registro.Valor = Math.Round(valorFloatDec, 2, MidpointRounding.ToEven).ToString(formatacaoDados);

                                _medidorAtual.Registros.Add(registro);
                            }

                            instanteInicial = instanteInicial.AddMinutes(5);

                        }

                        // Desconectar do Medidor
                        estado.DescricaoFuncao = "Desconectar do Medidor";
                        estado.DadosEnviar = desconectarMedidor;

                        envia(estado);
                        semafaroEnvio.WaitOne(timerOut);
                        mostraEnviados(estado);

                        recebe(estado);
                        semafaroRecebimento.WaitOne(timerOut);
                        mostraRecebidos(estado);

                        // Desconecta do servidor
                        desconecta(estado);
                        semafaroDesconexao.WaitOne(timerOut);

                        // Se chegou até aqui é que toda a leitura do medidor foi feita com sucesso!
                        _medidorAtual.SucessoComunicao = true;

                    }

                }
            }
            catch (SocketException se)
            {
                Console.WriteLine("Erro na Comunicação com o medidor: {0}", se.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("Exceção não esperada: {0}", e.ToString());
            }

        }

        /// <summary>
        /// Mostra informações dos dados enviados
        /// </summary>
        /// <param name="estado"></param>
        private void mostraEnviados(Estado estado)
        {
            if (estado.QuantidadesBytesEnviados > 0)
            {
                //Console.WriteLine("Dados Enviados [{0}] = {1}", Encoding.ASCII.GetString(estado.DadosEnviar, 0, estado.QuantidadesBytesEnviados), estado.DescricaoFuncao);
                Console.WriteLine("Dados Enviados [{0}]", estado.DescricaoFuncao);
            }
        }

        /// <summary>
        /// Mostra informações dos dados recebidos e faz o tratamento de erros na troca de frames entre o cliente e o servidor
        /// </summary>
        /// <param name="estado"></param>
        /// <returns></returns>
        private bool mostraRecebidos(Estado estado)
        {
            bool retorno = false;

            if (estado.QuantidadesBytesRecebidos > 0)
            {
                // Calcula o obtem o checkSum dos dados recebidos
                byte checkSumValor = Util.obterChecksum(estado.DadosRecebidos, estado.QuantidadesBytesRecebidos);

                // Verifica se houve erro de comunicação examinando o checkSum
                if (checkSumValor != estado.DadosRecebidos[estado.QuantidadesBytesRecebidos - 1])
                {

                    //Console.WriteLine("* ERROS * Dados Recebidos [{0}] = {1}", Encoding.ASCII.GetString(estado.DadosRecebidos, 0, estado.QuantidadesBytesRecebidos), estado.DescricaoFuncao);
                    Console.WriteLine("* ERROS * Dados Recebidos [{0}]", estado.DescricaoFuncao);

                    byte[] bytesErros = new byte[estado.QuantidadesBytesRecebidos];
                    Util.copiaBytes(estado.DadosRecebidos, ref bytesErros, 0, estado.QuantidadesBytesRecebidos - 1);
                    bytesErros[1] = 0xFF; // Seta o código da função como ERRO 0xFF
                    Util.definirChecksum(ref bytesErros);

                    // Envia de vota os valores informando o erro
                    int bytesEnviadosErros = estado.SocketCliente.Send(bytesErros);

                    byte[] bytesRecebeErros = new byte[128];
                    int bytesRecebidosErros = estado.SocketCliente.Receive(bytesRecebeErros);

                }
                else
                {
                    //Console.WriteLine("Dados Recebidos [{0}] = {1}", Encoding.ASCII.GetString(estado.DadosRecebidos, 0, estado.QuantidadesBytesRecebidos), estado.DescricaoFuncao);
                    Console.WriteLine("Dados Recebidos [{0}]", estado.DescricaoFuncao);
                    retorno = true;
                }
            }

            return retorno;

        }

        /// <summary>
        /// Conecta com o servidor de modo assíncrono
        /// </summary>
        /// <param name="estado"></param>
        private void connecta(Estado estado)
        {
            try
            {
                semafaroConexao.Reset(); // Reseta semáfaro de conexão
                Console.WriteLine("Iniciando conexão em {0}:{1}", estado.EndPoint.Address, estado.EndPoint.Port);
                Socket client = estado.SocketCliente;
                // Conecta de modo assícrono
                client.BeginConnect(estado.EndPoint, new AsyncCallback(connectaCallback), estado);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                semafaroConexao.Set(); // Caso ocorra algum erro libera semáfaro
            }
        }

        /// <summary>
        /// Método de conexão assícrona
        /// </summary>
        /// <param name="ar"></param>
        private void connectaCallback(IAsyncResult ar)
        {
            try
            {
                Estado estado = (Estado)ar.AsyncState;
                Socket client = estado.SocketCliente;
                client.EndConnect(ar);
                Console.WriteLine("Conectado em {0}", client.RemoteEndPoint.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                semafaroConexao.Set(); // libera semáfaro
            }
        }

        /// <summary>
        /// Envia dados ao medidor de forma assícrona
        /// </summary>
        /// <param name="estado"></param>
        private void envia(Estado estado)
        {
            try
            {
                semafaroEnvio.Reset();
                Console.WriteLine("Iniciando envio de dados em {0}:{1}", estado.EndPoint.Address, estado.EndPoint.Port);
                Socket client = estado.SocketCliente;
                client.BeginSend(estado.DadosEnviar, 0, estado.DadosEnviar.Length, SocketFlags.None, new AsyncCallback(enviaCallback), estado);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                semafaroEnvio.Set();
            }
        }

        /// <summary>
        /// Método de enviar dados de forma assícrona
        /// </summary>
        /// <param name="ar"></param>
        private void enviaCallback(IAsyncResult ar)
        {
            try
            {
                Estado estado = (Estado)ar.AsyncState;
                Socket client = estado.SocketCliente;
                estado.QuantidadesBytesEnviados = client.EndSend(ar);
                Console.WriteLine("Dados enviados em {0}", client.RemoteEndPoint.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                semafaroEnvio.Set();
            }
        }

        /// <summary>
        /// Inicia o recebimento dos dados de forma assícrona
        /// </summary>
        /// <param name="estado"></param>
        private void recebe(Estado estado)
        {
            try
            {
                semafaroRecebimento.Reset();
                Console.WriteLine("Iniciando recebimento de {0}:{1}", estado.EndPoint.Address, estado.EndPoint.Port);
                Socket client = estado.SocketCliente;
                estado.DadosRecebidos = new byte[128];
                client.BeginReceive(estado.DadosRecebidos, 0, estado.DadosRecebidos.Length, SocketFlags.None, new AsyncCallback(recebeCallback), estado);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                semafaroRecebimento.Set();
            }
        }

        /// <summary>
        /// Recebe dados de forma assícrona
        /// </summary>
        /// <param name="ar"></param>
        private void recebeCallback(IAsyncResult ar)
        {
            try
            {
                Estado estado = (Estado)ar.AsyncState;
                Socket client = estado.SocketCliente;
                estado.QuantidadesBytesRecebidos = client.EndReceive(ar);
                Console.WriteLine("Dados recebidos de {0}", client.RemoteEndPoint.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                semafaroRecebimento.Set();
            }
        }

        /// <summary>
        /// Inicia a desconexão com o medidor de forma assícrona
        /// </summary>
        /// <param name="estado"></param>
        private void desconecta(Estado estado)
        {
            try
            {
                semafaroDesconexao.Reset();
                Console.WriteLine("Iniciando desconexão de {0}:{1}", estado.EndPoint.Address, estado.EndPoint.Port);
                Socket client = estado.SocketCliente;
                client.BeginDisconnect(false, new AsyncCallback(desconectaCallback), estado);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                semafaroDesconexao.Set();
            }
        }

        /// <summary>
        /// Desconecta do medidor de forma assícrona
        /// </summary>
        /// <param name="ar"></param>
        private void desconectaCallback(IAsyncResult ar)
        {
            try
            {
                Estado estado = (Estado)ar.AsyncState;
                Socket client = estado.SocketCliente;
                client.Shutdown(SocketShutdown.Both);
                client.EndDisconnect(ar);
                Console.WriteLine("Desconectado de {0}", client.RemoteEndPoint.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                semafaroDesconexao.Set();
            }
        }

    }
}
