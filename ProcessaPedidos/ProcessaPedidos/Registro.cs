﻿using System;

namespace ProcessaPedidos
{
    /// <summary>
    /// Informações do registro lido a partir de um instante
    /// </summary>
    public class Registro
    {

        private DateTime _dataHora;

        /// <summary>
        /// Instante Lido no formato yyyy-MM-dd hh:mm:ss
        /// </summary>
        public DateTime DataHora
        {
            get
            {
                return _dataHora;
            }
            set
            {
                _dataHora = value;
            }
        }

        private string _valor;

        /// <summary>
        /// Valor lido
        /// </summary>
        public string Valor
        {
            get
            {
                return _valor;
            }
            set
            {
                _valor = value;
            }
        }

    }
}
